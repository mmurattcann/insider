<?php


namespace App\Helpers;


use App\Enums\PointEnum;
use App\Repositories\Classes\MatchRepository;
use App\Repositories\Classes\TeamRepository;

class ScoreCalculator
{

    public $teamRepository;
    public $matchRepository;
    public $pointEnum;
    public function __construct()
    {
        $this->teamRepository = new TeamRepository();
        $this->matchRepository = new MatchRepository();
        $this->pointEnum = new PointEnum();
    }

    public function playedCount($teamId, $week){
        return $this->teamRepository
            ->baseQuery()
            ->where("week", "=" , "week")
            ->where("team_one", "=", "$teamId")
            ->orWhere("team_two", "=", "$teamId")
            ->count();
    }

    public function calculate($week){
        $matches = $this->matchRepository->getAllByWeek($week);
        $data = [];

        foreach ($matches as $match){
            $scoreOne = $match->score_one;
            $scoreTwo = $match->score_two;

            $point = 0;

            if($scoreOne > $scoreTwo){
                $point = $scoreOne * $this->pointEnum::WINNER;
                $scoreTwo = 0;
            }
            if($scoreOne == $scoreTwo){
                $point = $this->pointEnum::DRAW;
            }
            if($scoreOne< $scoreTwo){
                $point = $this->pointEnum::LOSER;
            }
        }
    }

    public function pointCalculator(){

    }
}
