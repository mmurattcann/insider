<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $db = new DB();

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $db::table("teams")->truncate();

        $collection = [
            [
                "title" => "Chelsea",
                "description" => "A football team",
                "rank" => 1
            ],
            [
                "title" => "Arsenal",
                "description" => "A football team",
                "rank" => 2
            ],
            [
                "title" => "Manchester City",
                "description" => "A football team",
                "rank" => 3
            ],
            [
                "title" => "Liverpool",
                "description" => "A football team",
                "rank" => 4
            ],
        ];

        $db::table("teams")->insert($collection);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
