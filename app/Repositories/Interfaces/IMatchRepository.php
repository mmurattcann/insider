<?php


namespace App\Repositories\Interfaces;


use App\Models\Match;
use Illuminate\Database\Eloquent\Collection;

interface IMatchRepository
{
    public function getMatch(int $id): Match;
    public function getAll(): Collection;
    public function getAllByWeek(int $week);
}
