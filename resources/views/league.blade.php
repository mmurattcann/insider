@extends("layout._layout")

@push("css")
    <style>

        table{
            margin-top: 100px;
        }
        .leagueTable{

            border: 1px solid;
        }
        .leagueTable .resultsRow{
            border-left: 1px solid;
        }
        .leagueTable tbody {
            font-size: 13px;
        }

        .leagueTable tbody td[colspan="3"]{
            border-left: 1px solid;
        }

        .play-all-button{
            float: left;
        }
        .next-week-button{
            float: right;
        }
    </style>
@endpush

@section("content")
    <div class="row">
        <div class="col-md-8">
            <table class="table   leagueTable" >
                <thead>
                <tr>
                    <th colspan="7">
                        League Table
                    </th>
                    <th colspan="7" class="resultsRow">
                        Match Results
                    </th>
                </tr>
                <tr>
                    <th>Teams</th>
                    <th>PTS</th>
                    <th>P</th>
                    <th>W</th>
                    <th>D</th>
                    <th>L</th>
                    <th>GD</th>
                    <th colspan="7" class="resultsRow">
                        4'th Week Match Result
                    </th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Chelsea</td>
                        <td>10</td>
                        <td>4</td>
                        <td>3</td>
                        <td>1</td>
                        <td>0</td>
                        <td>11</td>
                        <td colspan="3"></td>
                        <td></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td>Arsenal</td>
                        <td>8</td>
                        <td>4</td>
                        <td>2</td>
                        <td>2</td>
                        <td>0</td>
                        <td>6</td>
                        <td colspan="3">Chelsea</td>
                        <td>3-2</td>
                        <td colspan="2">Liverpool</td>
                    </tr>
                    <tr>
                        <td>Manchester City</td>
                        <td>8</td>
                        <td>4</td>
                        <td>2</td>
                        <td>2</td>
                        <td>0</td>
                        <td>4</td>
                        <td colspan="3">Arsenal</td>
                        <td>3-3</td>
                        <td colspan="2">Manchester City</td>
                    </tr>
                    <tr>
                        <td>Liverpool</td>
                        <td>4</td>
                        <td>4</td>
                        <td>1</td>
                        <td>1</td>
                        <td>2</td>
                        <td>0</td>
                        <td colspan="3"></td>
                        <td></td>
                        <td colspan="2"></td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td  colspan="7"><button type="button" class="btn btn-secondary btn-sm play-all-button">Play All</button></td>
                        <td colspan="7"><button type="button" class="btn btn-secondary btn-sm next-week-button">Next Week</button></td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="col-md-4">
            <table class="table">
                <thead>
                    <tr>
                        <th colspan="4">
                            4'th Week Predictions Of Championship
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="2">
                            Chelsea
                        </td>
                        <td colspan="2">
                            %45
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Arsenal
                        </td>
                        <td colspan="2">
                            %25
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Manchester City
                        </td>
                        <td colspan="2">
                            %25
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Liverpool
                        </td>
                        <td colspan="2">
                            %5
                        </td>
                    </tr>

                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push("js")

@endpush
