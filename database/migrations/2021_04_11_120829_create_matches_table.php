<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("team_one");
            $table->unsignedBigInteger("team_two");
            $table->unsignedInteger("score_one");
            $table->unsignedInteger("score_two");
            $table->unsignedBigInteger("week");
            $table->timestamps();
            $table->foreign("team_one")
                ->references("id")
                ->on("teams")->onDelete("cascade");
            $table->foreign("team_two")
                ->references("id")
                ->on("teams")->onDelete("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
