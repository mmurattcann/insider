<?php


namespace App\Enums;


class PointEnum
{
    const WINNER = 3;
    const DRAW   = 1;
    const LOSER  = 0;
    const WEEK_COUNT = 8; // Team count x 2
}
