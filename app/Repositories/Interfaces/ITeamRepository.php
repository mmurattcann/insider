<?php


namespace App\Repositories\Interfaces;


use App\Models\Team;
use Illuminate\Database\Eloquent\Collection;

interface ITeamRepository
{
    public function getTeam(int $id): Team;
    public function getAll(string $order): Collection;

}
