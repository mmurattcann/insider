<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    public function firstTeam(){
        return $this->hasOne(Team::class, "team_one");
    }
    public function secondTeam(){
        return $this->hasOne(Team::class, "team_two");
    }
}
