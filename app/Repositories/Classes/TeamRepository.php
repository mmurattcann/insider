<?php


namespace App\Repositories\Classes;


use App\Models\Team;
use App\Repositories\Interfaces\ITeamRepository;
use Illuminate\Database\Eloquent\Collection;

class TeamRepository implements ITeamRepository
{
    private  $model;
    private  $relations;

    public function __construct()
    {
        $this->model = new Team();
        $this->relations = [];
    }

    public function baseQuery(){
        return $this->model::query()->with($this->relations);
    }
    public function getTeam(int $id): Team
    {
        return $this->model->find($id);
    }
    public function getAll(string $order): Collection
    {
        return $this->baseQuery()->orderBy("rank", "$order")->get();
    }

    public function getTeamIds($order= "ASC"){
        return $this->model->orderBy("rank","$order")->pluck("id");
    }
}
