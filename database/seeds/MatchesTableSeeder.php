<?php

use App\Enums\PointEnum;
use App\Repositories\Classes\TeamRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MatchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teamRepository = new TeamRepository();

        $db = new DB();

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $db::table("matches")->truncate();

        $weekCount = PointEnum::WEEK_COUNT;
        $matches = [];

        function getMatches($teams)
        {
            shuffle($teams);

            return call_user_func_array('array_combine', array_chunk($teams, sizeof($teams) / 2));
        }

        for ($counter = 1; $counter <= $weekCount; $counter++) {

            $teams = array_values($teamRepository->getTeamIds("ASC")->toArray());

            $match = getMatches($teams);

            foreach ($match as $key => $mtc){
                $matches[]=[
                    "team_one" => $key,
                    "team_two" => $mtc,
                    "score_one" => rand(0, 5),
                    "score_two" => rand(0, 5),
                    "week" => $counter
                ];
            }

        }

        $db::table("matches")->insert($matches);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
