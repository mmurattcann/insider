<?php


namespace App\Repositories\Classes;


use App\Models\Match;
use App\Repositories\Interfaces\IMatchRepository;
use Illuminate\Database\Eloquent\Collection;

class MatchRepository implements IMatchRepository
{
    private  $model;
    private  $relations;

    public function __construct()
    {
        $this->model = new Match();
        $this->relations = [];
    }

    public function baseQuery(){
        return $this->model::query()->with($this->relations);
    }
    public function getMatch(int $id): Match
    {
        return $this->model->find($id);
    }
    public function getAll(): Collection
    {
        return $this->baseQuery()->orderBy("rank", "ASC")->get();
    }

    public function getAllByWeek(int $week)
    {
        return $this->baseQuery()->where("week", "=", "$week")->get();
    }

}
